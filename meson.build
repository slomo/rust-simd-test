project('simd-runtime-test', 'rust',
  version : '0.0.1',
  meson_version : '>= 1.0.0',
  default_options : ['buildtype=debugoptimized',
                     'rust_std=2021'
                    ]
)

rustc = meson.get_compiler('rust')
if rustc.get_id() in ['rustc', 'clippy-driver rustc'] and not rustc.version().contains('nightly')
  error('nightly rustc required')
endif

do_things = static_library('do_things', 'do_things.rs',
  rust_args : ['-Cpanic=abort'],
  rust_crate_type : 'rlib',
  )

do_things_sse = static_library('do_things_sse', 'do_things.rs',
  rust_args : ['-Cpanic=abort', '-Ctarget-feature=+sse'],
  rust_crate_type : 'rlib',
  )

do_things_avx = static_library('do_things_avx', 'do_things.rs',
  rust_args : ['-Cpanic=abort', '-Ctarget-feature=+avx'],
  rust_crate_type : 'rlib',
  )

do_things_avx512f = static_library('do_things_avx512f', 'do_things.rs',
  rust_args : ['-Cpanic=abort', '-Ctarget-feature=+avx512f'],
  rust_crate_type : 'rlib',
  )

exe = executable('simd-runtime-test', 'main.rs',
  rust_args : ['-Cpanic=abort'],
  link_with : [do_things, do_things_sse, do_things_avx, do_things_avx512f],
)
