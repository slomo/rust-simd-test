#![feature(portable_simd)]

pub fn do_things(dst: &mut [f32], src: &[f32]) {
    use std::simd::f32x64;

    for (dst, src) in Iterator::zip(dst.chunks_exact_mut(64), src.chunks_exact(64)) {
        let mut d = f32x64::from_slice(dst);
        let s = f32x64::from_slice(src);
        d *= s;
        dst.copy_from_slice(&d.to_array());
    }
}
