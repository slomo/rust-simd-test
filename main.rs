#[inline(never)]
fn do_things(dst: &mut [f32], src: &[f32]) {
    if std::arch::is_x86_feature_detected!("avx512f") {
        do_things_avx512f::do_things(dst, src);
    } else if std::arch::is_x86_feature_detected!("avx") {
        do_things_avx::do_things(dst, src);
    } else if std::arch::is_x86_feature_detected!("sse") {
        do_things_sse::do_things(dst, src);
    } else {
        do_things::do_things(dst, src);
    }
}

fn main() {
    let s = vec![0.5; 1024];
    let mut d = vec![1.0; 1024];

    do_things(&mut d, &s);

    println!("{d:?}");
}
